#include "Civil.h"
#include <exception>

using std::string;
using std::vector;

Civil::Civil()
{
	head = 0;
}

void Civil::addCitizen(const vector<Citizen> &citizens)
{
	for (auto &c : citizens)
	{
		(*this)[c[u8"����"] + 1].push_back(c);
	}
}

vector<Citizen>& Civil::operator[](size_t age)
{
	if (age < 1 || age > 120) { throw std::exception("Age error"); }

	return this->age[(head + age - 1) % 120];
}

void Civil::nextYear()
{
	head = (head + 119) % 120;
	age[head].clear();
	// ToDo - Create New Generation
}