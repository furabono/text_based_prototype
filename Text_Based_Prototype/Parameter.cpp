#include "Parameter.h"

using std::string;

Parameter::Parameter(string type, string name)
{
	if (type == "Quantitative") { this->type = ParameterType::Quantitative; }
	else { this->type = ParameterType::Qualitative; }

	this->name = name;
}