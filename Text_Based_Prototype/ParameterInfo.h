#pragma once
#include <vector>
#include <map>
#include <string>

class ParameterInfo
{
public:
	ParameterInfo() = default;
	ParameterInfo(const std::vector<std::string> &parameters);
	size_t operator[](std::string name) const;

private:
	std::map<std::string, size_t> name_index_map;
};