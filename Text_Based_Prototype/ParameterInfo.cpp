#include "ParameterInfo.h"
#include <utility>

using std::string;
using std::vector;

ParameterInfo::ParameterInfo(const vector<string> &parameters)
{
	for (int i = 0; i < parameters.size(); ++i)
	{
		name_index_map.insert(std::make_pair(parameters[i], (size_t)i));
	}
}

size_t ParameterInfo::operator[](string name) const
{
	auto it = name_index_map.find(name);
	if (it == name_index_map.end()) { throw std::exception("Parameter Name Error"); }
	
	return it->second;
}