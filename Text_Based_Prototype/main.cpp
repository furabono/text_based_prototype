#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Json\json.h>
#include <rapidjson\document.h>
#include "Civil.h"
#include "ParameterInfo.h"

using namespace std;

bool jsoncpp(string filename, ParameterInfo &pi, Civil &civil)
{
	ifstream data(filename);

	Json::Reader reader;
	Json::Value root;

	if (!reader.parse(data, root))
	{
		cout << "Failed to open json file" << endl;
		return false;
	}
	cout << "loaded" << endl;

	{
		Json::Value parameter = root["Parameter"];
		vector<string> parameters;
		for (int i = 0; i < parameter.size(); ++i)
		{
			parameters.push_back(parameter[i]["Name"].asString());
		}

		pi = ParameterInfo(parameters);
	}

	vector<Citizen> citizens;
	{
		vector<int> parameters;
		Json::Value citizen = root["Citizen"];
		for (int i = 0; i < citizen.size(); ++i)
		{
			for (int j = 0; j < citizen[i].size(); ++j)
			{
				parameters.push_back(citizen[i][j].asInt());
			}
			citizens.push_back(Citizen(parameters, pi));
			parameters.clear();
		}
	}

	root.clear();

	civil.addCitizen(citizens);

	return true;
}

bool rapidjson(string filename, ParameterInfo &pi, Civil &civil)
{

}

int main()
{
	string filename = "C:\\Users\\eric9\\Documents\\Bitbucket\\CitizenGenerator\\CitizenGenerator\\bin\\Debug\\test.json";
	//cin >> filename;

	ParameterInfo pi;
	Civil civil;

	if (!jsoncpp(filename, pi, civil)) { return 0; }

	for (int i = 1; i <= AGE_MAX; ++i)
	{
		cout.width(6);
		cout << civil[i].size() << ' ';
		if (i % 10 == 0) { cout << endl; }
	}

	return 0;
}