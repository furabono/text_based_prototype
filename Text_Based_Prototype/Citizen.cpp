#include "Citizen.h"
#include "ParameterInfo.h"

using std::vector;
using std::string;

Citizen::Citizen(const vector<int> &parameters, ParameterInfo &pi) : parameterInfo(pi)
{
	this->parameters.assign(parameters.begin(), parameters.end());
}

int& Citizen::operator[](size_t parameterIndex)
{
	return parameters[parameterIndex];
}

int Citizen::operator[](size_t parameterIndex) const
{
	return parameters[parameterIndex];
}

int& Citizen::operator[](const string &parameterName)
{
	return parameters[parameterInfo[parameterName]];
}

int Citizen::operator[](const string &parameterName) const
{
	return parameters[parameterInfo[parameterName]];
}