#pragma once
#include <vector>
#include <string>

class ParameterInfo;
class Policy;

class Citizen
{
public:
	Citizen(const std::vector<int> &parameters, ParameterInfo &parameterInfo);
	int& operator[](size_t parameterIndex);
	int operator[](size_t parameterIndex) const;
	int& operator[](const std::string &parameterName);
	int operator[](const std::string &parameterName) const;
	//int evaluatePolicy(const Policy &policy);

private:
	std::vector<int> parameters;
	const ParameterInfo &parameterInfo;
};