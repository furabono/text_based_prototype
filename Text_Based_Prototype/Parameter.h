#pragma once
#include <string>

class Parameter
{
public:
	enum class ParameterType
	{
		Quantitative, Qualitative
	};

	Parameter(std::string type, std::string name);

private:
	ParameterType type;
	std::string name;
};