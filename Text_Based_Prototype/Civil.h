#pragma once
#include "Citizen.h"
#include <vector>

#define AGE_MAX (120)

class Civil
{
public:
	Civil();
	void addCitizen(const std::vector<Citizen> &citizens);
	std::vector<Citizen>& operator[](size_t age);
	void nextYear();

private:
	std::vector<Citizen> age[AGE_MAX];
	int head;
};